import {
	Command,
	HtmlRequestContentType,
	JsonRequestContentType,
	Query,
	XhrResponse,
} from "sophie-http";

// Overwrite for your settings
const localhost = 'https://67298ec3-1495-450b-b55c-fccaabf4ff51.mock.pstmn.io';

// Simple usage of a Query
// Instantiate Query
const query: Query = new Query();

// Set the API endpoint
query.setUrl(`${localhost}/api/posts`);

// Make the request, returns a Promise
query.getData(new HtmlRequestContentType())
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

// Simple usage of a Command
// Instantiate Command
const command: Command = new Command();

// Set the API endpoint
command.setUrl(`${localhost}/api/posts`);

command.saveData(new JsonRequestContentType(), { title: "New post", text: "Lorem Ipsum... " })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

command.updateData(new JsonRequestContentType(), { id: 1, title: "Updated title" })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

command.deleteData(new JsonRequestContentType(), { id: 1 })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

// Extend Query and Command

// Uri for endpoint posts
const URI_API_POSTS = `${localhost}/api/posts`;

// Query, also called reader, will only execute read queries on the Posts domain
export class PostsQuery extends Query
{
	public constructor ()
	{
		super();
		this.url = URI_API_POSTS;
	}
}

// Command, also called writer, will only execute writing commands on the Posts domain
export class PostsCommand extends Command
{
	public constructor ()
	{
		super();
		this.url = URI_API_POSTS;
	}
}

const postsQuery: Query = new PostsQuery();
const postsCommand: Command = new PostsCommand();

postsQuery.getData(new JsonRequestContentType())
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

postsCommand.saveData(new JsonRequestContentType(), { title: "New post", text: "Lorem Ipsum... " })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

postsCommand.updateData(new JsonRequestContentType(), { id: 1, title: "Updated title" })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});

postsCommand.deleteData(new JsonRequestContentType(), { id: 1 })
	.then((res: XhrResponse) => {
		console.log(res);
	})
	.catch((res: XhrResponse) => {
		console.log(res);
	});
