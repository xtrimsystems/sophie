# Examples Sophie HTTP

Open the file index.ts and read it. It is self-explanatory.

## INSTALLATION
In your terminal go to `sophie/examples/sophie-http` and run
```shell
yarn install
yarn compile
```

You will need to have running an API endpoint in order to test this.
Or use a mock server. I use Postman's Mock Server feature.
In the root of this example there are a couple of json files to import in Postman.
Then in postman go to the collection sophie-http-example and create a new mock server,
when required select the environment sophie-http-example. This will generate
an url you will need to enter in the index.ts overwritting the one in the variable `localhost`

## USAGE
Open the `index.html` under the `web` folder in your browser and go to the developers console.
You should see logs of the responses to the requests.

## Contributing [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
MIT License

Copyright (c) 2018 XtrimSystems

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
