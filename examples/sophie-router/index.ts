import { bubbleUpToTag } from "sophie-helpers";
import { AbstractApplication, AbstractResource, AbstractResourceRouter, Framework } from "sophie-router";

// Set RegExp with the paths of our application
const HOME_PATH = /^\/+$/g;
const PAGE_PATH = /^\/page\d?/g;
const ANOTHER_PAGE_PATH = /^\/another-page/g;

// ResourceRouter to handle the root path
class HomeResourceRouter extends AbstractResourceRouter
{
	protected canRoute (pathName: string): boolean
	{
		return !!pathName.match(HOME_PATH);
	}
}

// ResourceRouter to handle the "page" paths
class PageResourceRouter extends AbstractResourceRouter
{
	protected canRoute (pathName: string): boolean
	{
		return !!pathName.match(PAGE_PATH);
	}
}

// ResourceRouter to handle the "another-page" paths
class AnotherPageResourceRouter extends AbstractResourceRouter
{
	protected canRoute (pathName: string): boolean
	{
		return !!pathName.match(ANOTHER_PAGE_PATH);
	}
}

// Resource that will run if matches the root path
class HomeResource extends AbstractResource
{
	public getPath (): RegExp
	{
		return HOME_PATH;
	}

	public run (app: AbstractApplication, pathName?: string, search?: string): void
	{
		console.log('Run Homepage');
	}
}

// Resource that will run if matches the page1 path
class Page1Resource extends AbstractResource
{
	public getPath (): RegExp
	{
		return /page1/g;
	}

	public run (app: AbstractApplication, pathName?: string, search?: string): void
	{
		console.log('Run Page 1');
	}
}

// Resource that will run if matches the page2 path
class Page2Resource extends AbstractResource
{
	public getPath (): RegExp
	{
		return /page2/g;
	}

	public run (app: AbstractApplication, pathName?: string, search?: string): void
	{
		console.log('Run Page 2');
	}
}

// Resource that will run if matches the page2 path
class AnotherPageResource extends AbstractResource
{
	public getPath (): RegExp
	{
		return /another-page/g;
	}

	public run (app: AbstractApplication, pathName?: string, search?: string): void
	{
		console.log('Run Another Page');
	}
}

// Your application data
class Application extends AbstractApplication
{
	// Extend me ...
}

// Instantiate objects
const application: AbstractApplication = new Application(); // Create our application
const framework: Framework = Framework.createInstance(application); // Create the framework that will handle the routing

// Create some routers
const homeResourceRouter: AbstractResourceRouter = new HomeResourceRouter();
const pageResourceRouter: AbstractResourceRouter = new PageResourceRouter();
const anotherPageResourceRouter: AbstractResourceRouter = new AnotherPageResourceRouter();

// Create some resources for the routers
const homePage: AbstractResource = new HomeResource();
const page1: AbstractResource = new Page1Resource();
const page2: AbstractResource = new Page2Resource();
const anotherPage: AbstractResource = new AnotherPageResource();

// Add resources to routers
homeResourceRouter.addResource(homePage);
pageResourceRouter.addResource(page1);
pageResourceRouter.addResource(page2);
anotherPageResourceRouter.addResource(anotherPage);

// Register the routers in the framework
framework.registerResourceRouter(homeResourceRouter);
framework.registerResourceRouter(pageResourceRouter);
framework.registerResourceRouter(anotherPageResourceRouter);

// Run the application
framework.run(location.pathname, location.search);

// Extra - To avoid the refresh of the page when
// navigation via click on a link you need to handle
// the navigation, the next script is a suggestion
handleNavigation();

const HTTP_PROTOCOL: string = 'http://';
const HTTPS_PROTOCOL: string = 'https://';
const MAIL_TO_PROTOCOL: string = 'mailto:';

function handleNavigation (): void
{
	document.addEventListener('click', (event) =>
	{
		const link = bubbleUpToTag(event.target as HTMLElement || event.srcElement as HTMLElement, 'a');

		if (link)
		{
			const href = (link as HTMLLinkElement).getAttribute('href');
			if (
				href &&
				href.substr(0, HTTP_PROTOCOL.length) !== HTTP_PROTOCOL &&
				href.substr(0, HTTPS_PROTOCOL.length) !== HTTPS_PROTOCOL &&
				href.substr(0, MAIL_TO_PROTOCOL.length) !== MAIL_TO_PROTOCOL)
			{
				event.stopPropagation();
				event.preventDefault();

				history.pushState({}, '', href);

				framework.run(href);
			}
		}
	});

	window.onpopstate = () => {
		framework.run(location.pathname, location.search);
	};
}
