var express = require('express');
var app = express();

app.use('/', express.static(__dirname + '/web'));
app.use('/page1', express.static(__dirname + '/web/index.html'));
app.use('/page2', express.static(__dirname + '/web/index.html'));
app.use('/another-page', express.static(__dirname + '/web/index.html'));

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});
