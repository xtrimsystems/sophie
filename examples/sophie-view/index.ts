import { getHTMLElement } from "sophie-helpers";
import { MustacheHttp, MustacheString } from "sophie-mustache-template-engine";
import { AbstractRenderableView, AbstractView, DomEvent, TemplateEngine } from "sophie-view";

// Create a View extending the AbstractView
export class MyView extends AbstractView
{
	// Attach listeners to the view
	protected events: DomEvent[] = [
		{ name: 'click', queries: ['#view'], callback: this.onEventCallback },
	];

	public constructor (el: HTMLElement)
	{
		super(el);
	}

	public removeListeners (): void
	{
		this.detachEvents();
	}

	private onEventCallback (e: MouseEvent): void
	{
		console.log(e);
	}
}

const domElement = getHTMLElement(document.body, '#view');

const myView = new MyView(domElement);

// Remove listeners from view after 5 seconds
setTimeout(() => myView.removeListeners(), 5000);

// Create a View extending the AbstractRenderableView
export class MyRenderableView extends AbstractRenderableView
{
	protected events: DomEvent[] = [];

	public constructor (el: HTMLElement, templateEngine: TemplateEngine)
	{
		super(el, templateEngine);
	}
}

const domElementView2 = getHTMLElement(document.body, '#view2');
const domElementView3 = getHTMLElement(document.body, '#view3');

const viewRenderableWithString = new MyRenderableView(
	domElementView2,
	new MustacheString('<div>Hello {{name}}!!</div>'),
);

const viewRenderableWithHTMLTemplate = new MyRenderableView(
	domElementView3,
	new MustacheHttp('template.html'),
);

viewRenderableWithString.render({ name: 'world'})
	.then(() => console.log('Rendered'))
	.catch((error) => console.log(error));

viewRenderableWithHTMLTemplate.render({ name: 'world'})
	.then(() => console.log('Rendered'))
	.catch((error) => console.log(error));
