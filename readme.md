# Sophie
Typescript tools for frontend applications

Consists in the following sub-modules
- [Sophie Http](https://www.npmjs.com/package/sophie-http)
- [Sophie Router](https://www.npmjs.com/package/sophie-router)
- [Sophie View](https://www.npmjs.com/package/sophie-view)
- [Sophie Mustache Template Engine](https://www.npmjs.com/package/sophie-mustache-template-engine)
- [Sophie Helpers](https://www.npmjs.com/package/sophie-helpers)

You can use them separatlely base on your needs. Or all together to
enhance productivity while developing a SPA since they provide the basis.

[![npm version](https://badge.fury.io/js/sophie-http.svg)](https://badge.fury.io/js/sophie-http)
[![npm dependencies](https://david-dm.org/xtrimsystems/sophie-http.svg)](https://www.npmjs.com/package/sophie-http?activeTab=dependencies)
[![npm downloads](https://img.shields.io/npm/dm/sophie-http.svg)](https://www.npmjs.com/package/sophie-http)

## INSTALLATION

```bash
yarn add sophie
```

## Usage
Visit the readme of each module for a quick explanation on how to use them.

Then go to the examples folder, there you can see some examples, follow the readme instructions.

## Changelog
[Changelog](https://bitbucket.org/xtrimsystems/sophie-mustache-template-engine/src/master/CHANGELOG.md)

## Contributing [![contributions welcome](https://img.shields.io/badge/contributions-welcome-brightgreen.svg?style=flat)](https://github.com/dwyl/esta/issues)

## License
This software is licensed under the terms of the [MIT license](https://opensource.org/licenses/MIT). See [LICENSE](https://bitbucket.org/xtrimsystems/sophie-mustache-template-engine/src/master/LICENSE) for the full license.
